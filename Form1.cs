﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        bool red = true;
        int brojac = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonClick(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (red)
                b.Text = "X";
            else
                b.Text = "O";

            red = !red;
            b.Enabled = false;
            brojac++;
            provjeraPobjednika();

        }

        private void provjeraPobjednika()
        {
            bool pobjeda = false;
            if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
                pobjeda = true;
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
                pobjeda = true;
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
                pobjeda = true;
            else if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
                pobjeda = true;
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
                pobjeda = true;
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
                pobjeda = true;
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
                pobjeda = true;
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!A3.Enabled))
                pobjeda = true;

            if (pobjeda)
            {
                
                String pobjednik = "";
                if (red)
                {
                    pobjednik = Ime2.Text;
                    brojO.Text = (Int32.Parse(brojO.Text) + 1).ToString();
                }
                else
                {
                    pobjednik = Ime1.Text;
                    brojX.Text = (Int32.Parse(brojX.Text) + 1).ToString();
                    
                }
                MessageBox.Show(pobjednik + " je pobijedio", "Kraj");
                onemoguciNastavak();
            }
            else
            {
                if (brojac == 9)
                {
                    MessageBox.Show("Nerijeseno", "Kraj");
                    brojNerijeseno.Text = (Int32.Parse(brojNerijeseno.Text) + 1).ToString();
                }
            }

        }
        private void onemoguciNastavak()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch { }
        }

        private void newGame_Click(object sender, EventArgs e)
        {
            red = true;
            brojac = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                    newGame.Text = "Nova igra";
                }
                catch { }
            }
            

        }

        private void ulazak(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (red)
                    b.Text = "X";
                else
                    b.Text = "O";
            }
        }

        private void izlazak(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }
    }
}
